# Django Admin

## Resources:

Official Docs:

https://docs.djangoproject.com/en/2.2/ref/contrib/admin/

Must read:

* https://www.slideshare.net/lincolnloop/customizing-the-django-admin

Others:
* https://www.techiediaries.com/customize-django-admin/
* https://www.ibm.com/developerworks/opensource/library/os-django-admin/index.html


Note: Some of these articles are almost 10 years old. The concepts haven't changed much, but some of the details might have.

________

## Open source projects:

* Django Grapelli - https://django-grappelli.readthedocs.io/en/latest/
* ella - https://github.com/ella/ella

## Refer implementations from GitHub

* https://github.com/search?q=django+admin&type=Topics

________________

## Tasks:

* Order questions in reverse chronological order - Show recent questions at the top
* Add a filter which shows only unanswered questions
* Custom the "Update Question" feature in `admin` so that only the user who created the admin can edit the question.
* Add a Search feature so that users can search questions by title

