## Wagtail CMS - Sumant, Bhakti

https://wagtail.io/
https://github.com/wagtail/wagtail

## Django Packages - Gursharan, Venugopal

https://djangopackages.org/
https://github.com/djangopackages/djangopackages

## Django Oscar - Vaibhav, Ritesh, Sai Kumar

http://oscarcommerce.com/
https://django-oscar.readthedocs.io/en/releases-1.6/
https://github.com/django-oscar/django-oscar

## Misago - Anvesh, Paras

http://misago-project.org/
https://github.com/rafalp/Misago

__________

## Bootcamp - Shivi

* https://github.com/vitorfs/bootcamp

## Other projects

* https://djangopackages.org/grids/g/cms/

____________

Checklist:

* Understand what the project does
* Setup the project so that it runs locally
* Use the application
* Visualize the database schema using [`django_extensions`](https://medium.com/@yathomasi1/1-using-django-extensions-to-visualize-the-database-diagram-in-django-application-c5fa7e710e16)
* Observe all the URLs using `django_extensions` - `python manage.py show_urls`
* Go through all the django apps and understand the purpose of each app
* Understand what each third party library does by going through every line in  `requirements.txt`
* Add a small feature
* Add a bigger feature / fix a bug from the GitHub issues list

