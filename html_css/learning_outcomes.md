## Learning Outcomes

### HTML and CSS concepts

* What is the box model?
* What are block, inline-block and inline elements?
* What do `margin` and `padding` mean? What's the difference between the two?
* Units - vh, vw, em, px, rem vs em. When to use each one of them?
* Positioning - absolute, relative, static, fixed. What do they mean and when are they used?
* float - What does this property do? 
* Responsive Design - What is Responsive design? What are media queries?
* Layout - How to create layouts using Flex and Grid
* CSS Selectors - When do you apply the following types of selectors?
    - type selector
    - class selector
    - id selector
    - child selectors
    - descendent selector
    - Combining CSS classes - Can you apply two more classes to a single element?
* CSS Cascade, Inheritance and specificity - Which rule wins when multiple rules are applied to a single element?

### Chrome Dev tools

* How to view and debug all the styles that apply to an element from various rules? (Elements Tab)
* How to view the status of all the HTTP requests made by the browser? (Network tab)

