## Tutorial and Drills

https://learn.shayhowe.com/

Use codepen

* Building Your First Web Page
* Getting to Know HTML
* Getting to Know CSS New
* Opening the Box Model
* Positioning Content - Learn `relative`, `absolute`, `static`, `fixed` positioning
* Chrome Dev tools - (See Chrome Dev tools section)
* Drill: https://gitlab.com/mountblue/js/css-layout-drills
* Working with Typography
* Creating Lists
* Adding Media
* Flexbox: See "Learn Flexbox" section
* Complex Selectors
* Responsive Web Design
* Drill: https://gitlab.com/mountblue/js/responsive-layout-drill

Share the drills as Codepen collections

__________

## Chrome Dev tools

* https://www.youtube.com/watch?v=wcFnnxfA70g


* How to view and debug all the styles that apply to an element from various rules? (Elements Tab)
* How to view the status of all the HTTP requests made by the browser? (Network tab)

____________

## Learn Flexbox

* https://www.freecodecamp.org/news/learn-css-flexbox-in-5-minutes-b941f0affc34/
* https://css-tricks.com/flexbox


## References:

* https://cssreference.io/
* http://css-tricks.com/

_________

## Project: Brighton Times website:

Mockups:

https://drive.google.com/drive/folders/19q7wUU6kAJfe0lFuejMKp1BcCy6am2XM


Guidelines:

See `responsive_news_site_guidelines.md`

