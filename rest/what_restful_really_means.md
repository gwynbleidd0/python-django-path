## REST

https://codewords.recurse.com/issues/five/what-restful-actually-means

### What is it

* Stands for Representational State Transfer. (Just a fancy title for a fairly simple concept)

* A set of design principles to make network communication more scalable and flexible.

### History

Roy Fielding analyzed why the Web and HTTP were preferred over other protocols. And came up with a set of architectural principles, and called it REST. 

### Architectural constraints:

1. Client - Server: Must have one-to-one communication. Not an event based system. 
2. Stateless - Server treats each request as a standalone. Clients and servers don't keep track of each other's state. 
3. Uniform Interface
    1. Identification of resources: Each resource can be identified uniquely through a URI / URL
    2. Manipulation of resources through URI
    3. Self Descriptive messages: Message from client to server and vice versa has all the info needed to meaningfully parse the message. 
    4. Hypermedia: Data sent from server to client has info on what the client can do next. So, if you visit a shopping cart page after making the payment. You can track its order, cancel it, or maybe ask delivery to be made on a specific date. So these "behaviours" can be suggested by the server to the client.
Others:
4. Caching and layering: Resources can be cached by client or at various layers between client and server. 
5. Code on Demand: Server can send code to client which needs to be executed by the client. (for example, JS)
